﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleFS
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleKey key, RWkey;
            string path, consoleLine = "", command = "", commandPath = "", writeText = "";

            path = Directory.GetCurrentDirectory();
            FS.Path = path;

            Console.WriteLine("Current path: {0}", path);
            Console.WriteLine("Write your command: ");
            do
            {
                consoleLine = Console.ReadLine();
                command = consoleLine.Split(' ')[0].ToUpper();
                commandPath = String.Join(" ", consoleLine.Split(' ').Skip<string>(1));
                if (command == "WRITE")
                {
                    do
                    {
                        writeText = Console.ReadLine();
                        FS.method(command, commandPath, writeText);
                        Console.WriteLine("Press Esc to return to main program, press any key to continue writing...");
                    }
                    while ((RWkey = Console.ReadKey().Key) != ConsoleKey.Escape);
                }
                else
                {
                    FS.method(command, commandPath);
                }
                Console.WriteLine("Press Esc to exit, press any key to continue...");
            }
            while ((key = Console.ReadKey().Key) != ConsoleKey.Escape);
            

            Console.ReadKey();
        }
    }
}

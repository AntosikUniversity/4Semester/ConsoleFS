﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace ConsoleFS
{
    static class FS
    {
        static string path;

        public static string Path
        {
            get
            {
                return path;
            }
            set
            {
                path = value;
            }
        }

        public static void method(string name, string p, string text = "")
        {
            Console.WriteLine("{0} Command - path {1}", name, p);
            switch (name)
            {
                case "DIR": dir(p); break;
                case "CD": cd(p); break;
                case "EXEC": exec(p); break;
                case "WRITE": write(p, text); break;
                case "READ": read(p); break;
                case "TREE": tree(p); break;
                default: Console.WriteLine("Invalid command!"); return;
            }
            return;
        }

        
        public static void dir(string p = "")
        {
            string newpath = changePath(p);
            if (!Directory.Exists(newpath))
            {
                Console.WriteLine("DIR ERROR! Directory {0} not found!", newpath);
                return;
            }

            string[] Files = Directory.GetFiles(newpath);
            foreach (string file in Files)
                Console.WriteLine(file);

            string[] Directories = Directory.GetDirectories(newpath);
            foreach (string directory in Directories)
                Console.WriteLine(directory);
            
            return;
        }

        public static void cd(string p = "")
        {
            string newpath = changePath(p);
            if (!Directory.Exists(newpath))
            {
                Console.WriteLine("CD ERROR! Directory {0} not found!", newpath);
                return;
            }

            Path = newpath;
        }
        
        public static void exec(string p)
        {
            string newpath = changePath(p);
            if (!File.Exists(newpath))
            {
                Console.WriteLine("EXEC ERROR! File {0} not found!", newpath);
                return;
            }

            Process.Start(newpath);

            return;
        }

        public static void write(string p, string text)
        {
            string newpathdir = changePath(p + "/..");
            if (!Directory.Exists(newpathdir))
            {
                Console.WriteLine("WRITE ERROR! Directory {0} not found!", newpathdir);
                return;
            }
            string newpath = changePath(p);
            using (FileStream fs = new FileStream(newpath, FileMode.Open, FileAccess.ReadWrite))
            {
                fs.Seek(0, SeekOrigin.End);
                var sr = new StreamWriter(fs);
                sr.Write(text);
                sr.Flush();
                fs.Seek(0, SeekOrigin.Begin);
            }
            return;
        }

        public static void read(string p)
        {
            string newpath = changePath(p);
            if (!File.Exists(newpath))
            {
                Console.WriteLine("READ ERROR! File {0} not found!", newpath);
                return;
            }


            using (FileStream fs = new FileStream(newpath, FileMode.Open, FileAccess.ReadWrite))
            {
                string line;
                var sr = new StreamReader(fs);
                while ((line = sr.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }
            return;
        }

        public static void tree(string p = "")
        {
            string newpath = changePath(p);
            if (!Directory.Exists(newpath))
            {
                Console.WriteLine("TREE ERROR! Directory {0} not found!", newpath);
                return;
            }

            string[] Files = Directory.GetFiles(newpath);
            foreach (string file in Files)
                Console.WriteLine(file);

            string[] Directories = Directory.GetDirectories(newpath);
            foreach (string directory in Directories)
            {
                Console.WriteLine("\t {0}", directory);
                tree(directory);
            }

            return;
        }

        static string changePath(string p)
        {
            char[] separators = { '\\', '/' };
            List<string> pathSplit = Path.Split('\\').ToList<string>();
            List<string> newPathSplit = p.Split(separators).ToList<string>();

            if (p.StartsWith("/")) pathSplit.RemoveRange(1, pathSplit.Count - 1);

            foreach (string s in newPathSplit)
            {
                if (s == ".") continue;
                else if (s == "..") pathSplit.RemoveAt(pathSplit.Count - 1);
                else if (s.Contains(':')) return p;
                else pathSplit.Add(s);
            }
                    
            return String.Join("\\", pathSplit);
        }
    }
}
